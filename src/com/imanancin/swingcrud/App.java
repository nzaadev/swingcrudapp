package com.imanancin.swingcrud;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import com.imanancin.swingcrud.InputValidation.Builder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTable;

public class App extends JFrame {

	private JPanel contentPane;
	private JTextField jtxNama;
	private JTextField jtxAlamat;
	private JTable table;
	MyTableModel myTableModel;
	Siswa siswaEdit = null;
	JButton btnSimpan;
	JButton btnDelete;
	
	
	DB databaseDb = new DB();
	private JButton btnClearSelect;


	public static void main(String[] args) {
		
		
		 
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				 for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			            if ("Nimbus".equals(info.getName())) {
			                try {
								UIManager.setLookAndFeel(info.getClassName());
							} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
									| UnsupportedLookAndFeelException e) {
								e.printStackTrace();
							}
			                break;
			            }
			        }
				 
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public App() {

		// exit app apabila tombol close (x) di klik
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// posisi dan ukuran jframe
		setBounds(100, 100, 600, 600);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDataSiswa = new JLabel("Form Data Siswa");
		lblDataSiswa.setFont(new Font("Arial Black", Font.PLAIN, 13));
		lblDataSiswa.setBounds(105, 12, 237, 15);
		lblDataSiswa.setVerticalAlignment(SwingConstants.TOP);
		lblDataSiswa.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblDataSiswa);
		
		JLabel lblNama = new JLabel("Nama");
		lblNama.setFont(new Font("Arial", Font.BOLD, 13));
		lblNama.setBounds(34, 68, 70, 15);
		contentPane.add(lblNama);
		
		JLabel lblAlamat = new JLabel("Alamat");
		lblAlamat.setFont(new Font("Arial", Font.BOLD, 13));
		lblAlamat.setBounds(34, 126, 70, 15);
		contentPane.add(lblAlamat);
		
		jtxNama = new JTextField();
		jtxNama.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		jtxNama.setBounds(108, 58, 270, 36);
		contentPane.add(jtxNama);
		jtxNama.setColumns(10);
		
		jtxAlamat = new JTextField();
		jtxAlamat.setBounds(108, 116, 270, 36);
		contentPane.add(jtxAlamat);
		jtxAlamat.setColumns(10);
		
		btnSimpan = new JButton("Simpan");
		
		btnSimpan.setFont(new Font("Liberation Sans", Font.BOLD, 12));
		btnSimpan.setForeground(new Color(26, 95, 180));
		
		// aksi btn simpan
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				InputValidation.Builder statusNama = new InputValidation.Builder(jtxNama.getText())
						.minLength(2)
						.maxLength(10)
						.removeWhiteSpace()
						.setNotNull()
						.setLabel("Nama")
						.build();
				
				InputValidation.Builder statusAlamat = new InputValidation.Builder(jtxAlamat.getText())
						.minLength(2)
						.maxLength(10)
						.removeWhiteSpace()
						.setNotNull()
						.setLabel("Alamat")
						.build();
				
				if(statusAlamat.errors) {
					JOptionPane.showMessageDialog(null, statusAlamat.parseError());
					return;
				}
				
				if(statusNama.errors) {
					JOptionPane.showMessageDialog(null, statusNama.parseError());
					return;
				}
				

				if(siswaEdit == null) {
					// simpan
					myTableModel.add(new Siswa(statusNama.toString(), statusAlamat.toString(), 0));
					JOptionPane.showMessageDialog(null, "Data berhasil disimpan");
				} else {
					// update
					myTableModel.update(siswaEdit.getId(), new Siswa(statusNama.toString(), statusAlamat.toString(), siswaEdit.getId()));
					JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
				}
				
				jtxNama.setText("");
				jtxAlamat.setText("");
				
				
			}


		});
		
		btnSimpan.setBounds(74, 182, 117, 25);
		contentPane.add(btnSimpan);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jtxNama.setText("");
				jtxAlamat.setText("");
			}
		});
		btnReset.setFont(new Font("Liberation Sans", Font.BOLD, 12));
		btnReset.setForeground(new Color(165, 29, 45));
		btnReset.setBounds(241, 182, 117, 25);
		contentPane.add(btnReset);
	
		
		table = new JTable();
	
		table.setFont(new Font("", Font.PLAIN, 15));
		table.getTableHeader().setFont(new Font("Liberation Sans", Font.BOLD, 15));
		table.setDefaultEditor(Object.class, null);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()) {
					int id = (int) table.getValueAt(table.getSelectedRow(), 0);
					Siswa siswa = databaseDb.getSiswa(id);
					System.out.println(siswa.getNama());
					siswaEdit = siswa;
					btnDelete.setVisible(true);
					btnSimpan.setText("Update");
					jtxNama.setText(siswa.getNama());
					jtxAlamat.setText(siswa.getAlamat());
				}
			}
		});
		
		myTableModel = new MyTableModel();
		// setmodel buat custom table
		table.setModel(myTableModel);
		
		table.setFillsViewportHeight(true);
		table.setBounds(34, 242, 378, 300);
		table.setVisible(true);
		TableColumnModel tblm = table.getColumnModel();
		
		//sembunnyikan kolom id id
		tblm.getColumn(0).setWidth(0); 
		tblm.getColumn(0).setMinWidth(0); // harus sebelum setMaxWidth()
		tblm.getColumn(0).setMaxWidth(0);
		
		tblm.getColumn(1).setMaxWidth(50);
		
		
		// scrollpane supaya tabel bisa discroll
		JScrollPane scroolPane = new JScrollPane(table);
		scroolPane.setLocation(30, 250);
		scroolPane.setVisible(true);
		scroolPane.setSize(550, 300);
		
		contentPane.add(scroolPane);
		
		btnDelete = new JButton("Delete");
		btnDelete.setVisible(false);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// delete methods
				myTableModel.delete(siswaEdit.getId());
				siswaEdit = null;
				btnSimpan.setText("Simpan");
				jtxAlamat.setText("");
				jtxNama.setText("");
				
			}
		});
		btnDelete.setBounds(399, 181, 117, 25);
		contentPane.add(btnDelete);
		
		btnClearSelect = new JButton("clear select");
		btnClearSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				table.getSelectionModel().clearSelection();
				siswaEdit = null;
				btnSimpan.setText("Simpan");
				jtxAlamat.setText("");
				jtxNama.setText("");
				btnDelete.setVisible(false);
			}
		});
		btnClearSelect.setBounds(399, 120, 117, 25);
		contentPane.add(btnClearSelect);
	}
}
