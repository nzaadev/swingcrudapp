package com.imanancin.swingcrud;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class InputValidation {
	

	public static class Builder {
		
		ArrayList<String> error = new ArrayList<>();
		boolean errors = false;
		String label;
		String input;
		
		public Builder(String input) {
			this.input = input;
		}
		
		Builder setLabel(String label) {
			this.label = label;
			return this;
		}
		
		Builder removeWhiteSpace() {
			input = input.trim();
			return this;
		}
		
		Builder maxLength(int length) {
			if(this.input.length() > length) {
				error.add("input :label maksimal " + length + " karakter");
			}
			
			return this;
		}
		
		Builder setNotNull() {
			if(input.isEmpty()) {
				error.add(":label tidak boleh kosong");
			}
			return this;
		}
		
		Builder minLength(int length) {
			if(this.input.length() < length) {
				error.add("input :label minimal " +length + " karakter");
			}
			
			return this;
		}
		
		Builder build() {
			if(error.size() > 0) {
				errors = true;
			}
			
			return this;
			
		}
		
		String parseError() {
			return String.join("\n", error).replace(":label", this.label);
		}
		
		public String toString() {
			return this.input.toString();
		}

	}
}
