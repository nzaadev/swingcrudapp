package com.imanancin.swingcrud;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DB {
	
	static Connection mysqlConnection = null;
	
	final String DB_HOST = "localhost";
	final String DB_NAME = "javadb";
	final String DB_USERNAME = "root";
	final String DB_PASSWORD = "1";
	
	final String tbl = "siswa";
	final String namaTbl = "nama";
	final String alamatTbl = "alamat";
	final String idTbl = "id";
	
	public DB() {
		initDB();
	}
	
	void initDB() {
		if(mysqlConnection == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				mysqlConnection = DriverManager.getConnection(
						"jdbc:mysql://"+DB_HOST+":3306/"+DB_NAME,
						DB_USERNAME,DB_PASSWORD
						);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}

		}
	}
	
	List<Siswa> getAllSiswa() {
		
		List<Siswa> tmpList = new ArrayList<>();
		try {
			Statement statement = mysqlConnection.createStatement();
			// executeQuery untuk exec query yg mengembalikan result
			ResultSet resultSet = statement.executeQuery("select * from siswa");		
			while (resultSet.next()) {
				tmpList.add(
						new Siswa(
								resultSet.getString(namaTbl), 
								resultSet.getString(alamatTbl), 
								resultSet.getInt(idTbl)
								)
						);
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		return tmpList;
	}
	
	Siswa getSiswa(int id) {
		try {
			
			PreparedStatement statement = mysqlConnection.prepareStatement("SELECT * FROM siswa WHERE id = ? LIMIT 1");
			statement.setInt(1, id);
			ResultSet resultSet =  statement.executeQuery();
			
			Siswa siswa = null;
			
			while(resultSet.next()) {
				siswa = new Siswa(resultSet.getString(namaTbl), resultSet.getString(alamatTbl), resultSet.getInt(idTbl));
			}
			
			return siswa;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	void insertSiswa(Siswa siswa) {
		PreparedStatement pStatement;
		try {
			pStatement = mysqlConnection.prepareStatement("INSERT INTO siswa (nama, alamat) values(?,?)");
			pStatement.setString(1, siswa.getNama());		
			pStatement.setString(2, siswa.getAlamat());
			// executeUpdate untuk exec query yg tdk mengembalikan result
			pStatement.executeUpdate();
			pStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	void updateSiswa(int id, Siswa siswa) {
		PreparedStatement pStatement;
		try {
			pStatement = mysqlConnection.prepareStatement("UPDATE siswa SET nama=?, alamat=? where id=?");
			pStatement.setString(1, siswa.getNama());
			pStatement.setString(2, siswa.getAlamat());
			pStatement.setInt(3, id);
			// executeUpdate untuk exec query yg tdk mengembalikan result
			pStatement.executeUpdate();
			pStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	void deleteSiswa(int id) {
		PreparedStatement pStatement;
		try {
			pStatement = mysqlConnection.prepareStatement("DELETE from siswa where id=?");
			pStatement.setInt(1, id);
			pStatement.executeUpdate();
			// executeUpdate untuk exec query yg tdk mengembalikan result
			pStatement.executeUpdate();
			pStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

}
