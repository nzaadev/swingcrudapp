package com.imanancin.swingcrud;

public class Siswa {
	
	private String nama;
	private String alamat;
	private int id;
	
	public Siswa(String nama, String alamat, int id) {
		this.nama = nama;
		this.alamat = alamat;
		this.id = id;
	}
	
	String getNama() {
		return nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public int getId() {
		return id;
	}

	
	

}
