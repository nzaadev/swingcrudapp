package com.imanancin.swingcrud;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;


public class MyTableModel extends AbstractTableModel {
	
	DB db = new DB();
	String[] headerName = new String[] {"", "No","Nama","Alamat"};
	
	private static final long serialVersionUID = 1L;
	public List<Siswa> siswas = new ArrayList<>();
	
	
	public MyTableModel() {
		siswas = db.getAllSiswa();
	}
	

	@Override
	public int getColumnCount() {
		return headerName.length;
	}

	@Override
	public int getRowCount() {
		return siswas.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int colIndex) {
		Siswa siswa = siswas.get(rowIndex);
		switch (colIndex) {
		case 0: return siswa.getId();
		case 1:
			return rowIndex + 1;
		case 2:
			return siswa.getNama();
		case 3:
			return siswa.getAlamat();

		}
		return null;
	}
	
	public void add(Siswa siswa) {
		db.insertSiswa(siswa);
		getAllData();
		fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
	}
	
	public void delete(int id) {
		db.deleteSiswa(id);
		fireTableRowsDeleted(0, getRowCount());
		getAllData();
	}
	
	public void update(int id, Siswa siswa) {
		db.updateSiswa(id, siswa);
		fireTableRowsUpdated(0, getRowCount());
		getAllData();
	}
	
	
	void getAllData() {
		siswas.clear();
		siswas.addAll(db.getAllSiswa());
	}
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return super.isCellEditable(rowIndex, columnIndex);
	}


//
//	public void remove(Siswa siswa) {
//		if(siswas.contains(siswa)) {
//			int row = siswas.indexOf(siswa);
//			siswas.remove(row);
//			fireTableRowsInserted(row, row);
//		}
//	}
//	
//	public void update() {
//		siswas.clear();
//		siswas = db.getSiswa();
//		fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
//	}
	
	public String getColumnName(int column) {
		return headerName[column];
	}
	
	

}
